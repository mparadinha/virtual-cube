#version 330 core

in vec2 uv;

out vec4 FragColor;

uniform sampler2D tex;

const vec3 text_color = vec3(0, 0, 0);

void main() {
    float alpha = texture(tex, uv).r;
    FragColor = vec4(text_color, alpha);
}
