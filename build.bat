@set libs=user32.lib kernel32.lib gdi32.lib vcruntime.lib msvcrt.lib shell32.lib opengl32.lib glfw3.lib
@set linker_stuff=/LIBPATH:..\libs

@set src_files=..\src\main.cpp ..\src\load_gl.cpp ..\src\shaders.cpp ..\src\common.cpp ..\src\mesh.cpp ..\src\common.cpp ..\src\gl_math.cpp ..\src\stb_truetype.cpp ..\src\text.cpp ..\src\camera.cpp ..\src\stb_image.cpp

@set build_dir=.obj

@if not exist %build_dir% (
    mkdir %build_dir%
)

pushd %build_dir%
cl %src_files% /std:c++latest /Zi /I ..\libs %libs% /link %linker_stuff%
@set cl_errorlvl=%ERRORLEVEL%
popd

@if %cl_errorlvl% neq 0 (
    @echo build.bat finished :(
    @exit /B %cl_errorlvl%
) else (
    @move %build_dir%\main.exe main.exe
    @echo build.bat finished :^)
    @echo/
    main.exe
)
