#include "common.h"
#include "load_gl.h"
#include "shaders.h"

#include <string.h>
#include <assert.h>

uint32_t make_and_compile_shader(std::string filename, const char* c_str, int len, uint32_t type) {
    Buffer filebuf = {0};
    if(c_str == NULL) {
        filebuf = read_whole_file(filename.c_str());
        c_str = (const char*) filebuf.data;
        len = filebuf.size;
    }

    uint32_t id = glCreateShader(type);

    glShaderSource(id, 1, &c_str, &len);
    glCompileShader(id);

    // check for compilation errors
    int32_t success = 0;
    char error_msg[1024] = {};
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if(success == GL_FALSE) {
        glGetShaderInfoLog(id, sizeof(error_msg), NULL, error_msg);
        print_debug("(%s) shader compile error: %s\n", filename.c_str(), error_msg);
    }

    if (filebuf.data) free(filebuf.data);

    return id;
}

void prefetch_uniform_locations(Shader* shader) { 
    shader->m_uniform = glGetUniformLocation(shader->id, "m");
    shader->p_uniform = glGetUniformLocation(shader->id, "p");
    shader->v_uniform = glGetUniformLocation(shader->id, "v");
}

Shader make_shader_program(std::string name, std::string dir) {
    std::string file = dir + "/" + name;

    Shader shader;
    shader.vert_id = make_and_compile_shader(file + ".vert", NULL, 0, GL_VERTEX_SHADER);
    shader.frag_id = make_and_compile_shader(file + ".frag", NULL, 0, GL_FRAGMENT_SHADER);

    shader.id = glCreateProgram();
    glAttachShader(shader.id, shader.vert_id);
    glAttachShader(shader.id, shader.frag_id);
    glLinkProgram(shader.id);
    // TODO: check for compilation errors

    prefetch_uniform_locations(&shader);

    return shader;
}

Shader make_shader_program_src(const char* vert_src, const char* frag_src) {
    Shader shader = {
        .vert_id = make_and_compile_shader("[custom]", vert_src, strlen(vert_src), GL_VERTEX_SHADER),
        .frag_id = make_and_compile_shader("[custom]", frag_src, strlen(frag_src), GL_FRAGMENT_SHADER),
    };

    shader.id = glCreateProgram();
    glAttachShader(shader.id, shader.vert_id);
    glAttachShader(shader.id, shader.frag_id);
    glLinkProgram(shader.id);

    prefetch_uniform_locations(&shader);

    return shader;
}
