#pragma once

#include <stdio.h>
#include <string.h>
#include <vector>

#include "gl_math.h"

struct Texture {
    uint32_t w, h;
    uint32_t id;
};

Texture create_texture(const char* path);
Texture create_texture(uint8_t* data, uint32_t w, uint32_t h,
                       uint32_t num_channels);

struct Character {
    int32_t codepoint;
    // I would use a uint32_t for codepoints but stb_truetype uses int32_t
    // but unicode only needs 21 bits anyway so it's fine

    int32_t w, h, down, advance;  // these are in pixels
    vec2 topleft, bottomright;    // opengl texture coordinates in atlas
};

struct Font {
Texture atlas;
    Character charinfo[96];
};

Font create_font(const char* fontpath, float pixelsize);

struct TextBufferInfo {
uint32_t n_chars;
    uint32_t n_verts;
    uint32_t total_size;

    uint32_t vao, vbo;

    float final_width;
    vec2 vertical_extent;

    std::vector<vec4> data;
};

TextBufferInfo create_string_verts(Font font, const char* str, vec2 start,
                                   float div, float ratio);
void free_text_buffer(TextBufferInfo buffer);
