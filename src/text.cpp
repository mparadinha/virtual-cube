#include "stb_image.h"
#include "stb_truetype.h"

#include <algorithm>
#include <assert.h>
#include <fstream>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "load_gl.h"
#include "text.h"

void debug_save_to_file(const char *filename, uint8_t *bitmap, uint32_t w, uint32_t h);

Font create_font(const char *filepath, float size) {
    // init the font
    Buffer font_file = read_whole_file(filepath);
    uint8_t *font_data = font_file.data;
    stbtt_fontinfo font;
    stbtt_InitFont(&font, font_data, 0);

    float scale = stbtt_ScaleForPixelHeight(&font, size);

    int ascent, descent, linegap;
    stbtt_GetFontVMetrics(&font, &ascent, &descent, &linegap);
    ascent *= scale;
    descent *= scale;

    printf("size: %f, ascent - descent: %d\n", size, ascent - descent);

    // pack into a bitmap
    const uint32_t w = 512, h = 512;
    uint8_t bitmap[w * h];

    Font retval;

    const uint32_t xpadding = 2, ypadding = 1;
    int cur_x = 0, cur_y = 0;
    for(uint32_t i = 0; i < 96; i++) {
        int c = i + ' '; // space is the first character we care about
        if(i == 95) c = 0x1f914;

        int x1, y1, x2, y2;
        stbtt_GetCodepointBitmapBox(&font, c, scale, scale, &x1, &y1, &x2, &y2);

        int end_x = cur_x + (x2 - x1) + xpadding;
        if (end_x >= 512) {
            cur_x = 0;
            cur_y += (ascent - descent) + ypadding;
        }
        if (cur_y + (ascent - descent) >= 512) {
            print_debug("font atlas is not big enough\n");
            break;
        }

        uint32_t offset = cur_x + (cur_y * w);
        stbtt_MakeCodepointBitmap(&font, bitmap + offset, x2 - x1, y2 - y1, w,
                                  scale, scale, c);

        int char_advance;
        stbtt_GetCodepointHMetrics(&font, c, &char_advance, 0);
        // space ' ' has 0 width, this ^ tells how much to move forward

        retval.charinfo[i] = {
            .codepoint = c,
            .w = x2 - x1,
            .h = y2 - y1,
            .down = y2,
            .advance = (int)(char_advance * scale),
            .topleft = {cur_x / float(w), (cur_y / float(h))},
            .bottomright = {(cur_x + (x2 - x1)) / float(w),
                            ((cur_y + (y2 - y1)) / float(h))}};

        cur_x += (x2 - x1) + xpadding;
    }

    debug_save_to_file("font.ppm", bitmap, w, h);

    free(font_data);

    retval.atlas = create_texture(bitmap, 512, 512, 1);

    return retval;
}

Texture create_texture(uint8_t *data, uint32_t width, uint32_t height,
                       uint32_t num_channels) {
    uint32_t id;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    unsigned int formats[] = {0, GL_RED, 0, GL_RGB, GL_RGBA};
    glTexImage2D(GL_TEXTURE_2D, 0, formats[num_channels], width, height, 0,
                 formats[num_channels], GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    return Texture{width, height, id};
}

Texture create_texture(const char *path) {
    int width, height, num_channels;
    unsigned char *data = stbi_load(path, &width, &height, &num_channels, 0);
    assert(data);
    Texture ret_val = create_texture(data, width, height, num_channels);
    stbi_image_free(data);
    return ret_val;
}

TextBufferInfo allocate_text_buffer(uint32_t n_chars, uint8_t *data) {
    uint32_t n_verts = n_chars * 6;
    uint32_t total_size = n_verts * 2 * sizeof(vec2);

    uint32_t vao, vbo;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, total_size, data, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4,
                          (void *)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4,
                          (void *)8);
    glEnableVertexAttribArray(1);

    return {.n_chars = n_chars,
            .n_verts = n_verts,
            .total_size = total_size,
            .vao = vao,
            .vbo = vbo};
}

void free_text_buffer(TextBufferInfo buffer) {
    glDeleteVertexArrays(1, &buffer.vao);
    glDeleteBuffers(1, &buffer.vbo);
}

TextBufferInfo create_string_verts(Font font, const char *str, vec2 start,
                                   float div, float ratio) {
    uint32_t str_len = strlen(str);
    vec2 *data = (vec2 *)malloc(sizeof(vec2) * 12 * str_len);
    assert(data);

    float lowest = 0, highest = 0;

    float cur_x = start[0];
    for (auto idx = 0; idx < strlen(str); idx++) {
        if (str[idx] == '\n') continue;

        Character c = font.charinfo[str[idx] - ' '];
        vec2 tl = c.topleft, br = c.bottomright;

        vec2 thischar[6 * 2] = {
            {0.0f, 0.0f},             {tl[0], br[1]},
            {(float)c.w, 0.0f},       br,
            {(float)c.w, (float)c.h}, {br[0], tl[1]},

            {0.0f, 0.0f},             {tl[0], br[1]},
            {(float)c.w, (float)c.h}, {br[0], tl[1]},
            {0.0f, (float)c.h},       tl
        };

        for (uint32_t i = 0; i < 6 * 2; i += 2) {
            if (c.codepoint == 'j') thischar[i][0] -= 0.4f * c.w; // hack to make j look normal

            thischar[i][1] -= c.down; // how it goes below the baseline
            thischar[i] /= div;       // apply scale to fit on screen
            thischar[i][0] /= ratio;  // correct for screen ratio
            thischar[i] += vec2({cur_x, start[1]});

            lowest = std::min(lowest, thischar[i][1] - start[1]);
            highest = std::max(highest, thischar[i][1] - start[1]);
        }

        for (uint32_t i = 0; i < 6 * 2; i++) { data[12 * idx + i] = thischar[i]; }

        cur_x += (c.advance / div) / ratio;
    }

    TextBufferInfo buffer = allocate_text_buffer(str_len, (uint8_t *)data);
    buffer.final_width = cur_x - start[0];
    buffer.vertical_extent = {lowest, highest};

    buffer.data = std::vector<vec4>((vec4*) data, (vec4*) (data + 12 * str_len));
    // free(data);

    return buffer;
}

void debug_save_to_file(const char *filename, uint8_t *bitmap, uint32_t w,
                        uint32_t h) {
    FILE *file = fopen(filename, "w");
    fprintf(file, "P2\n%d %d\n255\n", w, h);
    uint32_t cur = 0;
    for (uint32_t x = 0; x < w; x++) {
        for (uint32_t y = 0; y < h; y++) {
            fprintf(file, "%d ", bitmap[cur++]);
        }
        fprintf(file, "\n");
    }
    fclose(file);
}
