#ifdef _WIN32
#include <windows.h>
// yeah I know, windows.h has macros with these names. fml
#undef near
#undef far
#undef whereeveryouare
#endif

#include "camera.h"
#include "gl_math.h"
#include "load_gl.h"
#include "mesh.h"
#include "text.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

const uint32_t win_w = 1000;
const uint32_t win_h = 600;

GLFWwindow* window;

float scroll_offset = 0;

// order is: F, B, L, R, D, U
//  indices: 0, 1, 2, 3, 4, 5
uint8_t faces[6][9] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1},
    {2, 2, 2, 2, 2, 2, 2, 2, 2},
    {3, 3, 3, 3, 3, 3, 3, 3, 3},
    {4, 4, 4, 4, 4, 4, 4, 4, 4},
    {5, 5, 5, 5, 5, 5, 5, 5, 5},
};

void rotate_face_cw(uint8_t face[9]) {
    uint8_t new_face[9] = {
        face[2], face[5], face[8],
        face[1], face[4], face[7],
        face[0], face[3], face[6],
    };
    for (uint32_t i = 0; i < 9; i++) {
        face[i] = new_face[i];
    }
}

void rotate_face_ccw(uint8_t face[9]) {
    uint8_t new_face[9] = {
        face[6], face[3], face[0],
        face[7], face[4], face[1],
        face[8], face[5], face[2],
    };
    for (uint32_t i = 0; i < 9; i++) {
        face[i] = new_face[i];
    }
}

void cycle_top_row(uint8_t face_indices[4]) {
    uint8_t saved_row[3] = {
        faces[face_indices[3]][6],
        faces[face_indices[3]][7],
        faces[face_indices[3]][8],
    };

    for (uint32_t i = 0; i < 4; i++) {
        for (uint32_t k = 0; k < 3; k++) {
            uint8_t tmp = faces[face_indices[i]][6 + k];
            faces[face_indices[i]][6 + k] = saved_row[k];
            saved_row[k] = tmp;
        }
    }
}

void print_faces(uint8_t cube[6][9]) {
    for (uint32_t i = 0; i < 6; i++) {
        for (uint32_t k = 0; k < 9; k++) {
            printf("%u ", cube[i][k]);
        }
        printf("\n");
    }
}

void cube_rotation(char c) {
    uint8_t new_face_order[6];
    switch(c) {
        case 'x': {
            rotate_face_cw(faces[5]);
            rotate_face_cw(faces[5]);
            rotate_face_cw(faces[1]);
            rotate_face_cw(faces[1]);
            uint8_t order[6] = {4, 5, 2, 3, 1, 0};
            memcpy(new_face_order, order, sizeof(order));
            rotate_face_cw(faces[3]);
            rotate_face_ccw(faces[2]);
        } break;
        case 'y': {
            uint8_t order[6] = {3, 2, 0, 1, 4, 5};
            memcpy(new_face_order, order, sizeof(order));
            rotate_face_cw(faces[5]);
            rotate_face_ccw(faces[4]);
        } break;
        case 'z': {
            rotate_face_cw(faces[2]);
            rotate_face_cw(faces[3]);
            rotate_face_cw(faces[4]);
            rotate_face_cw(faces[5]);
            uint8_t order[6] = {0, 1, 4, 5, 3, 2};
            memcpy(new_face_order, order, sizeof(order));
            rotate_face_cw(faces[0]);
            rotate_face_ccw(faces[1]);
        } break;
    }

    uint8_t old_state[6][9];
    memcpy(old_state, faces, sizeof(uint8_t) * 6 * 9);

    for (uint32_t i = 0; i < 6; i++) {
        for (uint32_t k = 0; k < 9; k++) {
            faces[i][k] = old_state[new_face_order[i]][k];
        }
    }
}

void rotate_up_cw() {
    rotate_face_cw(faces[5]);
    uint8_t row_cycle[4] = {0, 2, 1, 3};
    cycle_top_row(row_cycle);
}

void rotate_up_ccw() {
    rotate_face_ccw(faces[5]);
    uint8_t row_cycle[4] = {0, 3, 1, 2};
    cycle_top_row(row_cycle);
}

void glfw_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(action != GLFW_RELEASE) { return; }

    switch(key) {
        case GLFW_KEY_J: { // U
            rotate_up_cw();
        } break;
        case GLFW_KEY_F: { // U'
            rotate_up_ccw();
        } break;

        case GLFW_KEY_K: { // R
            cube_rotation('z');
            cube_rotation('z');
            cube_rotation('z');
            rotate_up_cw();
            cube_rotation('z');
        } break;
        case GLFW_KEY_L: { // R'
            cube_rotation('z');
            cube_rotation('z');
            cube_rotation('z');
            rotate_up_ccw();
            cube_rotation('z');
        } break;

        case GLFW_KEY_D: { // L'
            cube_rotation('z');
            rotate_up_ccw();
            cube_rotation('z');
            cube_rotation('z');
            cube_rotation('z');
        } break;
        case GLFW_KEY_S: { // L
            cube_rotation('z');
            rotate_up_cw();
            cube_rotation('z');
            cube_rotation('z');
            cube_rotation('z');
        } break;

        case GLFW_KEY_H: { // F
            cube_rotation('x');
            rotate_up_cw();
            cube_rotation('x');
            cube_rotation('x');
            cube_rotation('x');
        } break;
        case GLFW_KEY_G: { // F'
            cube_rotation('x');
            rotate_up_ccw();
            cube_rotation('x');
            cube_rotation('x');
            cube_rotation('x');
        } break;

        case GLFW_KEY_A: { // D
            cube_rotation('z');
            cube_rotation('z');
            rotate_up_cw();
            cube_rotation('z');
            cube_rotation('z');
        } break;
        case GLFW_KEY_SEMICOLON: { // D'
            cube_rotation('z');
            cube_rotation('z');
            rotate_up_ccw();
            cube_rotation('z');
            cube_rotation('z');
        } break;

        case GLFW_KEY_X: {
            cube_rotation('x');
        } break;
        case GLFW_KEY_Y: {
            cube_rotation('y');
        } break;
        case GLFW_KEY_Z: {
            cube_rotation('z');
        } break;

        default: printf("key, scancode: %d, %d\n", key, scancode);
    }
}

void glfw_scroll_callback(GLFWwindow* window, double xoff, double yoff) {
    scroll_offset = yoff;
}

uint32_t make_quad_vao() {
    float verts[] = {0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                     1.0, 1.0, 0.0, 0.0, 1.0, 0.0};
    uint32_t indices[] = {0, 1, 2, 0, 2, 3};

    uint32_t vao, vbo, ebo;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*) 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*) 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    return vao;
} 

void draw_cube(uint32_t quad_vao, Shader shader, uint8_t faces[6][9]) {
    glUseProgram(shader.id);
    glBindVertexArray(quad_vao);

    mat4 face_trans;

    uint32_t m_loc = glGetUniformLocation(shader.id, "m");
    uint32_t c_loc = glGetUniformLocation(shader.id, "color");

    vec3 colors[] = {
        {1, 1, 1}, // white
        {1, 1, 0}, // yellow
        {0, 1, 0}, // green
        {0, 0, 1}, // blue
        {1, 0, 0}, // red
        {1, 0.5, 0}, // orange
    };

    // front face
    face_trans = translation_matrix({0, 0, 1.5});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0})
                * face_trans;
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[0][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }

    // back face
    face_trans = translation_matrix({0, 0, -1.5});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = face_trans
                * rotation_matrix({0, 1, 0}, M_PI)
                * translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0});
                
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[1][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }

    // left face
    face_trans = translation_matrix({-1.5, 0, 0});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = face_trans
                * rotation_matrix({0, 1, 0}, -M_PI / 2)
                * translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0});
                
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[2][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }

    // right face
    face_trans = translation_matrix({1.5, 0, 0});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = face_trans
                * rotation_matrix({0, 1, 0}, M_PI / 2)
                * translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0});
                
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[3][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }

    // down face
    face_trans = translation_matrix({0, -1.5, 0});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = face_trans
                * rotation_matrix({1, 0, 0}, M_PI / 2)
                * translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0});
                
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[4][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }

    // upface
    face_trans = translation_matrix({0, 1.5, 0});
    for (int32_t i = -1; i <= 1; i++) {
        for (int32_t j = -1; j <= 1; j++) {
            mat4 m = face_trans
                * rotation_matrix({1, 0, 0}, -M_PI / 2)
                * translation_matrix({(float) i - 0.5f, (float) j - 0.5f, 0});
                
            glUniformMatrix4fv(m_loc, 1, GL_FALSE, (float*) &m);
            vec3 c = colors[faces[5][(i + 1) + (j + 1) * 3]];
            glUniform3f(c_loc, c[0], c[1], c[2]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }
    }
}

void set_cwd() {
    #ifdef _WIN32
        char buf[MAX_PATH];
        GetModuleFileName(NULL, buf, MAX_PATH);
        auto exe_dir = std::string(buf).substr(0, std::string(buf).find_last_of("\\/"));
        SetCurrentDirectory(exe_dir.c_str());
    #else
        assert(!"STUB for set_cwd for non windows machines");
    #endif
}

int main() {
    set_cwd();

    assert(glfwInit());
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    window = glfwCreateWindow(win_w, win_h, "i3floatme", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, glfw_key_callback);
    glfwSetScrollCallback(window, glfw_scroll_callback);
    glfwSwapInterval(1);

    load_gl();
    printf("opengl version: %s\n", glGetString(GL_VERSION));

    glViewport(0, 0, win_w, win_h);
    // need to enable this for the text rendering
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // some more state that we never change
    // glEnable(GL_CULL_FACE);
    glClearColor(0.2, 0.87, 0.82, 1);

    float fov = 60.0f, near = 0.1f, far = 1000.0f;
    float ratio = (float)win_w / (float)win_h;
    float yaw = 18.3, pitch = -28.58;
    vec3 cam_pos = {2.17, 3.76, 6.65};
    Camera camera;
    camera.yaw = yaw;
    camera.pitch = pitch;
    camera.projection = projection_matrix(fov, ratio, near, far);
    camera.view =
        view_matrix(cam_pos, (yaw * M_PI / 180.0f), (pitch * M_PI / 180.0f));
    camera.position = cam_pos;
    camera.locked = false;

    Font font = create_font("Gidole-Regular.ttf", 50.0f);
    Shader text_shader = make_shader_program("text", ".");

    uint32_t quad_vao = make_quad_vao();
    mat4 quad_m = identity_matrix();
    Shader cube_shader = make_shader_program("cubefaces", ".");

    float dt = 0, last_time = glfwGetTime();
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        update_camera_centered(&camera, window, scroll_offset, dt);
        scroll_offset = 0;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        set_camera_uniforms(camera, cube_shader);
        draw_cube(quad_vao, cube_shader, faces);

        glfwSwapBuffers(window);

        float tmp = glfwGetTime();
        dt = tmp - last_time;
        last_time = tmp;
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
