#pragma once

#include <stdint.h>

void load_gl();

// OpenGL constants
#define GL_NONE 0
#define GL_FALSE 0
#define GL_TRUE 1
#define GL_POINTS 0x0000
#define GL_LINES 0x0001
#define GL_LINE_STRIP 0x0003
#define GL_BLEND 0x0be2
#define GL_DEPTH_TEST 0x0b71
#define GL_LEQUAL 0x0203
#define GL_ONE_MINUS_SRC_ALPHA 0x0303
#define GL_SRC_ALPHA 0x0302
#define GL_FRONT_AND_BACK 0x0408
#define GL_LINE_SMOOTH 0x0b20
#define GL_CULL_FACE 0x0b44
#define GL_UNSIGNED_SHORT 0x1403
#define GL_UNSIGNED_INT 0x1405
#define GL_DEPTH_COMPONENT 0x1902
#define GL_LINE 0x1b01
#define GL_FILL 0x1b02
#define GL_VERSION 0x1f02
#define GL_TEXTURE0 0x84c0
#define GL_TEXTURE_2D 0x0de1
#define GL_LINEAR 0x2601
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_NEAREST 0x2600
#define GL_CLAMP 0x2900
#define GL_REPEAT 0x2901
#define GL_CLAMP_TO_EDGE 0x812f
#define GL_RED 0x1903
#define GL_RGB 0x1907
#define GL_RGBA	0x1908
#define GL_FLOAT 0x1406
#define GL_DEPTH_COMPONENT16 0x81a5
#define GL_ALIASED_LINE_WIDTH_RANGE 0x846e
#define GL_FRAGMENT_SHADER 0x8b30
#define GL_VERTEX_SHADER 0x8b31
#define GL_ARRAY_BUFFER 0x8892
#define GL_STATIC_DRAW 0x88e4
#define GL_FRAMEBUFFER_COMPLETE 0x8cd5
#define GL_DEPTH_ATTACHMENT 0x8d00
#define GL_FRAMEBUFFER 0x8d40
#define GL_TRIANGLES 0x0004
#define GL_DEPTH_BUFFER_BIT 0x00000100
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_COMPILE_STATUS 0x8b81
#define GL_LUMINANCE 0x1909
#define GL_UNSIGNED_BYTE 0x1401
#define GL_DEBUG_OUTPUT 0x92e0

// OpenGL typedefs (for reference)
typedef unsigned int	GLenum;
typedef unsigned char	GLboolean;
typedef unsigned int	GLbitfield;
typedef void		GLvoid;
typedef signed char	GLbyte;		/* 1-byte signed */
typedef short		GLshort;	/* 2-byte signed */
typedef int		GLint;		/* 4-byte signed */
typedef unsigned char	GLubyte;	/* 1-byte unsigned */
typedef unsigned short	GLushort;	/* 2-byte unsigned */
typedef unsigned int	GLuint;		/* 4-byte unsigned */
typedef int		GLsizei;	/* 4-byte signed */
typedef float		GLfloat;	/* single precision float */
typedef double		GLdouble;	/* double precision float */

// declare all the functions we need
#define FUNC_PTR(OUT, IN, STR) \
typedef OUT (*FP_##STR) IN; \
extern FP_##STR STR;

FUNC_PTR(void*, (const char*), glXGetProcAddress)
FUNC_PTR(const uint8_t*, (uint32_t name), glGetString)
FUNC_PTR(void, (int32_t x, int32_t y, int32_t w, int32_t h), glViewport)
FUNC_PTR(void, (uint32_t id), glActiveTexture)
FUNC_PTR(void, (uint32_t n, uint32_t* textures), glGenTextures)
FUNC_PTR(void, (uint32_t target, uint32_t texture), glBindTexture)
FUNC_PTR(void, (uint32_t target, uint32_t pname, uint32_t param), glTexParameteri)
FUNC_PTR(void, (uint32_t target, int level, int internalformat, int w, int h, int border,
    uint32_t format, uint32_t type, const void* data), glTexImage2D)
FUNC_PTR(void, (uint32_t target), glGenerateMipmap)
FUNC_PTR(uint32_t, (uint32_t type), glCreateShader)
FUNC_PTR(void, (uint32_t id), glCompileShader)
FUNC_PTR(uint32_t, (void), glCreateProgram)
FUNC_PTR(void, (uint32_t, int, const char**, const int*), glShaderSource)
FUNC_PTR(void, (uint32_t prog_id, uint32_t shader_id), glAttachShader)
FUNC_PTR(void, (uint32_t id), glLinkProgram)
FUNC_PTR(void, (int, uint32_t*), glGenVertexArrays)
FUNC_PTR(void, (int, uint32_t*), glGenBuffers)
FUNC_PTR(void, (uint32_t), glBindVertexArray)
FUNC_PTR(void, (uint32_t type, uint32_t id), glBindBuffer)
FUNC_PTR(void, (uint32_t target, long size, const void* data, uint32_t usage),
    glBufferData)
FUNC_PTR(void, (uint32_t idx, int size, uint32_t type, bool norm, uint32_t stride,
    const void* ptr), glVertexAttribPointer)
FUNC_PTR(void, (uint32_t idx), glEnableVertexAttribArray)
FUNC_PTR(void, (uint32_t mask), glClear)
FUNC_PTR(void, (uint32_t id), glUseProgram)
FUNC_PTR(void, (uint32_t type, int first, uint32_t count), glDrawArrays)
FUNC_PTR(void, (int32_t location, uint32_t value), glUniform1i)
FUNC_PTR(void, (int32_t location, uint32_t value0, uint32_t value1), glUniform2i)
FUNC_PTR(void, (int32_t location, float v1), glUniform1f)
FUNC_PTR(void, (int32_t location, float v1, float v2, float v3), glUniform3f)
FUNC_PTR(void, (int32_t location, float v1, float v2, float v3, float v4), glUniform4f)
FUNC_PTR(void, (int32_t location, int32_t count, uint32_t transpose, const float* data), glUniformMatrix4fv)
FUNC_PTR(int, (uint32_t prog_id, const char*), glGetUniformLocation)
FUNC_PTR(void, (float, float, float, float), glClearColor)
FUNC_PTR(void, (uint32_t shader, uint32_t pname,  int32_t *params), glGetShaderiv)
FUNC_PTR(void, (uint32_t shader, int32_t size, int32_t* len, char* info_log), glGetShaderInfoLog)
FUNC_PTR(uint32_t, (void), glGetError)
FUNC_PTR(void, (void (*callback)(uint32_t source, uint32_t type, uint32_t id, uint32_t severity, int32_t length, const char* message, const void* user_param), void* user_param), glDebugMessageCallback)
FUNC_PTR(void, (uint32_t), glEnable)
FUNC_PTR(void, (uint32_t sfactor, uint32_t dfactor), glBlendFunc)
FUNC_PTR(void, (uint32_t func), glDepthFunc)
FUNC_PTR(void, (uint32_t mode, int32_t count, uint32_t type, const void* indices), glDrawElements)
FUNC_PTR(void, (uint32_t face, uint32_t mode), glPolygonMode)
FUNC_PTR(void, (int, uint32_t*), glDeleteVertexArrays)
FUNC_PTR(void, (int, uint32_t*), glDeleteBuffers)
FUNC_PTR(void, (int n, uint32_t* buffers), glGenFramebuffers)
FUNC_PTR(void, (uint32_t target, uint32_t framebuffer), glBindFramebuffer)
FUNC_PTR(uint32_t, (uint32_t target), glCheckFramebufferStatus)
FUNC_PTR(void, (uint32_t target, uint32_t attachment, uint32_t texture, int level), glFramebufferTexture)
FUNC_PTR(void, (uint32_t buf), glDrawBuffer) 
FUNC_PTR(void, (uint32_t buf), glReadBuffer) 
FUNC_PTR(void, (float width), glLineWidth)
FUNC_PTR(void, (uint32_t type, float*), glGetFloatv)
#undef FUNC_PTR
