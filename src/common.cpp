#include "common.h"

#include <stdarg.h>
#include <stdio.h>

void print_debug(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    char buffer[512];
    vsnprintf(buffer, 512, fmt, args);
    va_end(args);

    printf("[debug_warning] %s", buffer);
    //debug_warnings.push_back(std::string(buffer));
}

Buffer read_whole_file(const char* filepath) {
    FILE* handle = fopen(filepath, "rb");
    if(!handle) { print_debug("error opening '%s'\n", filepath); }

    fseek(handle, 0, SEEK_END);
    uint64_t file_size = ftell(handle);
    fseek(handle, 0, SEEK_SET);

    uint8_t* data = (uint8_t*) malloc(file_size);
    fread(data, 1, file_size, handle);

    fclose(handle);

    return {
        .data = data,
        .size = file_size,
        .pos = 0
    };
}
