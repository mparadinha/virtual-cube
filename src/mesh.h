#include <vector>

#include "gl_math.h"

struct Mesh {
    std::vector<vec3> verts;
    std::vector<vec3> normals;
    std::vector<uint32_t> indices;

    std::vector<vec2> texcoords;

    uint32_t vao, vbo, ebo;
};

void load_into_gpu(Mesh* objmesh);
