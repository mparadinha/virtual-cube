#pragma once

#include <stdint.h>
#include <iostream>
#include <string>

#define _USE_MATH_DEFINES // windows needs this to define M_PI and other stuff
#include <math.h>

float to_radians(float degrees);

template<uint32_t N, typename T>
struct vec {
    T data[N];

    T& operator[](uint32_t i) { return data[i]; }

    vec<N, T>& operator+=(vec<N, T> other) {
        for(auto i = 0; i < N; i++) data[i] += other[i];
        return *this;
    }
    vec<N, T>& operator-=(vec<N, T> other) {
        for(auto i = 0; i < N; i++) data[i] -= other[i];
        return *this;
    }

    vec<N, T>& operator*=(T mult) {
        for(auto i = 0; i < N; i++) data[i] *= mult;
        return *this;
    }

    vec<N, T>& operator/=(T div) {
        for(auto i = 0; i < N; i++) data[i] /= div;
        return *this;
    }

    std::string str() {
        std::string s = "(";
        for(auto i = 0; i < N; i++) {
            s += std::to_string(data[i]);
            s += (i != N - 1) ? ", " : ")";
        }
        return s;
    }
};

using vec2 = vec<2, float>;
using vec3 = vec<3, float>;
using vec4 = vec<4, float>;
using ivec2 = vec<2, int32_t>;

template<uint32_t N, typename T>
bool operator==(vec<N, T> a, vec<N, T> b) {
    for(auto i = 0; i < N; i++) { if(a[i] != b[i]) return false; }
    return true;
}

template<uint32_t N, typename T>
bool operator!=(vec<N, T> a, vec<N, T> b) { return !(a == b); }

template<uint32_t N, typename T>
vec<N, T> operator-(vec<N, T> v) {
    vec<N, T> res;
    for(auto i = 0; i < N; i++) { res[i] = -v[i]; }
    return res;
}

template<uint32_t N, typename T>
vec<N, T> operator+(vec<N, T> a, vec<N, T> b) {
    vec<N, T> res;
    for(auto i = 0; i < N; i++) { res[i] = a[i] + b[i]; }
    return res;
}

template<uint32_t N, typename T>
vec<N, T> operator-(vec<N, T> a, vec<N, T> b) {
    vec<N, T> res;
    for(auto i = 0; i < N; i++) { res[i] = a[i] - b[i]; }
    return res;
}

template<uint32_t N, typename T>
vec<N, T> operator*(float s, vec<N, T> v) {
    vec<N, T> res;
    for(auto i = 0; i < N; i++) { res[i] = s * v[i]; }
    return res;
}

template<uint32_t N, typename T>
vec<N, T> operator/(vec<N, T> v, float s) {
    vec<N, T> res;
    for(auto i = 0; i < N; i++) { res[i] = v[i] / s; }
    return res;
}


template<uint32_t N, typename T>
T dot_product(vec<N, T> a, vec<N, T> b) {
    T sum = 0;
    for(auto i = 0; i < N; i++) { sum += a[i] * b[i]; }
    return sum;
}

template<uint32_t N, typename T>
float length(vec<N, T> v) { return sqrt(dot_product(v, v)); }

template<uint32_t N, typename T>
vec<N, T> normalize(vec<N, T> v) { return v / sqrt(dot_product(v, v)); }


template<uint32_t N, typename T>
void print(vec<N, T> v) {
    std::cout << "(";
    for(auto i = 0; i < N; i++) {
        if(i == N - 1) std::cout << v[i];
        else std::cout << v[i] << ", ";
    }
    std::cout << ")";
}

vec3 cross_product(vec3 a, vec3 b);


// NOTE: we store the elements as a column major matrix because
// that's what OpenGL uses. (this means the first 4 elements are the
// the first column, etc.)
struct mat4 {
    vec4 columns[4];
    vec4& operator[](uint32_t i) { return columns[i]; }

    mat4& operator*=(float f) {
        for(auto i = 0; i < 4; i++)
            for(auto j = 0; j < 4; j++) (*this)[i][j] *= f;
        return *this;
    }
    mat4& operator/=(float f) {
        (*this) *= (1 / f);
        return *this;
    }
};

mat4 operator-(mat4 m);
mat4 operator-(mat4 a, mat4 b);
mat4 operator+(mat4 a, mat4 b);
mat4 operator*(mat4 a, mat4 b);
vec4 operator*(mat4 m, vec4 v);

void print(mat4 m);
mat4 identity_matrix();
mat4 transpose(mat4 m);
mat4 inverse(mat4 _m);


mat4 projection_matrix(float fov, float ratio, float near, float far);
mat4 ortho_projection_matrix(float left, float right, float bottom, float top, float near, float far);

// note: yaw and pitch are in radians
// yaw starts at -x and rotates according to +y
// pitch is the angle with the xz plane
mat4 view_matrix(vec3 pos, float yaw, float pitch);
mat4 look_at_matrix(vec3 eye, vec3 center, vec3 up);
mat4 translation_matrix(vec3 delta);
mat4 scaling_matrix(vec3 scale);

// note: axis must be normalized
// note: angle is in radians
mat4 rotation_matrix(vec3 axis, float angle);

struct quat {
    float data[4]; // x, y, z, w
    float& operator[](uint32_t i) { return data[i]; }
};

quat operator*(float s, quat q);
quat operator/(quat q, float s);

float length(quat q);
quat normalize(quat q);

mat4 to_matrix(quat q);
