#include <assert.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

#include "load_gl.h"

#define DECL_FUNC(STR) \
FP_##STR STR;

DECL_FUNC(glXGetProcAddress)
DECL_FUNC(glGetString)
DECL_FUNC(glViewport)
DECL_FUNC(glActiveTexture)
DECL_FUNC(glGenTextures)
DECL_FUNC(glBindTexture)
DECL_FUNC(glTexParameteri)
DECL_FUNC(glTexImage2D)
DECL_FUNC(glGenerateMipmap)
DECL_FUNC(glCreateShader)
DECL_FUNC(glCompileShader)
DECL_FUNC(glCreateProgram)
DECL_FUNC(glShaderSource)
DECL_FUNC(glAttachShader)
DECL_FUNC(glLinkProgram)
DECL_FUNC(glGenVertexArrays)
DECL_FUNC(glGenBuffers)
DECL_FUNC(glBindVertexArray)
DECL_FUNC(glBindBuffer)
DECL_FUNC(glBufferData)
DECL_FUNC(glVertexAttribPointer)
DECL_FUNC(glEnableVertexAttribArray)
DECL_FUNC(glClear)
DECL_FUNC(glUseProgram)
DECL_FUNC(glDrawArrays)
DECL_FUNC(glUniform1i)
DECL_FUNC(glUniform2i)
DECL_FUNC(glUniform1f)
DECL_FUNC(glUniform3f)
DECL_FUNC(glUniform4f)
DECL_FUNC(glUniformMatrix4fv)
DECL_FUNC(glGetUniformLocation)
DECL_FUNC(glClearColor)
DECL_FUNC(glGetShaderiv)
DECL_FUNC(glGetShaderInfoLog)
DECL_FUNC(glGetError)
DECL_FUNC(glDebugMessageCallback)
DECL_FUNC(glEnable)
DECL_FUNC(glBlendFunc)
DECL_FUNC(glDepthFunc)
DECL_FUNC(glDrawElements)
DECL_FUNC(glPolygonMode)
DECL_FUNC(glDeleteVertexArrays)
DECL_FUNC(glDeleteBuffers)
DECL_FUNC(glGenFramebuffers)
DECL_FUNC(glBindFramebuffer)
DECL_FUNC(glCheckFramebufferStatus)
DECL_FUNC(glFramebufferTexture)
DECL_FUNC(glDrawBuffer) 
DECL_FUNC(glReadBuffer) 
DECL_FUNC(glLineWidth) 
DECL_FUNC(glGetFloatv) 
#undef DECL_FUNC

void gl_error_msg_callback(uint32_t source, uint32_t type, uint32_t id, uint32_t severity, int32_t length, const char* message, const void* user_param) {
    if(severity == 0x826b) /* GL_DEBUG_SEVERITY_NOTIFICATION */ return;

    printf("OpenGL debug: %s\n", message);
    printf("severity: 0x%x\ntype: 0x%x\nid: 0x%x\n", severity, type, id);

    // this was meant to stop the program from running when a shader failed to compile
    // but this will actually exit before our normal error checking in shaders.h is run
    // so we end up not seeing the error message. therefore it is, for now, commented out
    //if(id == 0x000a) /* GL_INVALID_OPERATION */ exit(0);
}

#ifdef _WIN32
void* fp_loader(HMODULE lib, const char* name) {
        void* func = wglGetProcAddress(name);
        if (func) return func;
        else return GetProcAddress(lib, name);
}
#else
void* fp_loader(void* lib, const char* name) {
        return dlsym(lib, name);
}
#endif


void load_gl() {
    #ifdef _WIN32
        HMODULE lib = LoadLibraryA("opengl32.dll");
    #else
        void* lib = dlopen("libGL.so", RTLD_NOW | RTLD_GLOBAL);
    #endif
    assert(lib);

    // this macro assumes that the function was already declared
    #define LOAD_FUNC(LIB, FUNC) \
    FUNC = (FP_##FUNC) fp_loader(LIB, #FUNC); assert(FUNC);
    //LOAD_FUNC(lib, glXGetProcAddress)
    LOAD_FUNC(lib, glGetString)
    LOAD_FUNC(lib, glViewport)
    LOAD_FUNC(lib, glActiveTexture)
    LOAD_FUNC(lib, glGenTextures)
    LOAD_FUNC(lib, glBindTexture)
    LOAD_FUNC(lib, glTexParameteri)
    LOAD_FUNC(lib, glTexImage2D)
    LOAD_FUNC(lib, glGenerateMipmap)
    LOAD_FUNC(lib, glCreateShader)
    LOAD_FUNC(lib, glCompileShader)
    LOAD_FUNC(lib, glCreateProgram)
    LOAD_FUNC(lib, glShaderSource)
    LOAD_FUNC(lib, glAttachShader)
    LOAD_FUNC(lib, glLinkProgram)
    LOAD_FUNC(lib, glGenVertexArrays)
    LOAD_FUNC(lib, glGenBuffers)
    LOAD_FUNC(lib, glBindVertexArray)
    LOAD_FUNC(lib, glBindBuffer)
    LOAD_FUNC(lib, glBufferData)
    LOAD_FUNC(lib, glVertexAttribPointer)
    LOAD_FUNC(lib, glEnableVertexAttribArray)
    LOAD_FUNC(lib, glClear)
    LOAD_FUNC(lib, glUseProgram)
    LOAD_FUNC(lib, glDrawArrays)
    LOAD_FUNC(lib, glUniform1i)
    LOAD_FUNC(lib, glUniform2i)
    LOAD_FUNC(lib, glUniform1f)
    LOAD_FUNC(lib, glUniform3f)
    LOAD_FUNC(lib, glUniform4f)
    LOAD_FUNC(lib, glUniformMatrix4fv)
    LOAD_FUNC(lib, glGetUniformLocation)
    LOAD_FUNC(lib, glClearColor)
    LOAD_FUNC(lib, glGetShaderiv)
    LOAD_FUNC(lib, glGetShaderInfoLog)
    LOAD_FUNC(lib, glGetError)
    LOAD_FUNC(lib, glDebugMessageCallback)
    LOAD_FUNC(lib, glEnable)
    LOAD_FUNC(lib, glBlendFunc)
    LOAD_FUNC(lib, glDepthFunc)
    LOAD_FUNC(lib, glDrawElements)
    LOAD_FUNC(lib, glPolygonMode)
    LOAD_FUNC(lib, glDeleteVertexArrays)
    LOAD_FUNC(lib, glDeleteBuffers)
    LOAD_FUNC(lib, glGenFramebuffers)
    LOAD_FUNC(lib, glBindFramebuffer)
    LOAD_FUNC(lib, glCheckFramebufferStatus)
    LOAD_FUNC(lib, glFramebufferTexture)
    LOAD_FUNC(lib, glDrawBuffer)
    LOAD_FUNC(lib, glReadBuffer)
    LOAD_FUNC(lib, glLineWidth)
    LOAD_FUNC(lib, glGetFloatv)
    #undef LOAD_FUNC

    #ifdef _WIN32
        FreeLibrary(lib);
    #else
        dlclose(lib);
    #endif

    //glEnable(GL_DEBUG_OUTPUT);
    //glDebugMessageCallback(gl_error_msg_callback, NULL);
}
