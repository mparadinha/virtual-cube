#include "camera.h"

#define CLAMP(x, l, h) (x < l ? l : (x > h ? h : x))

void set_camera_uniforms(Camera camera, Shader shader, const char* p_str, const char* v_str) {
    glUniformMatrix4fv(glGetUniformLocation(shader.id, p_str), 1, GL_FALSE, (float*)&camera.projection);
    glUniformMatrix4fv(glGetUniformLocation(shader.id, v_str), 1, GL_FALSE, (float*)&camera.view);
}

void update_camera_centered(Camera* camera, GLFWwindow* window, float scroll, float dt) {
    //printf("camera = {\n  yaw = %f, pitch: %f\n  position: (%f, %f, %f)\n}\n",
    //    camera->yaw, camera->pitch,
    //    camera->position[0], camera->position[1], camera->position[2]
    //);
    static bool moving = false;
    if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE && scroll == 0.0f) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        moving = false;
        return;
    }

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    static double last_xpos = 0, last_ypos = 0;
    double xpos = 0, ypos = 0;
    glfwGetCursorPos(window, &xpos, &ypos);
    if(!moving) {
        moving = true;
        last_xpos = xpos; last_ypos = ypos;
    }
    // note: using the convention that theta is angle from z-axis and phi is
    // angle from x-axis.
    // note: moving mouse upwards would give negative delta (because y pixel
    // coordinates grow down the screen) but that is actually what we want.
    double mult = 0.1;
    double delta_theta = mult * (last_ypos - ypos);
    double delta_phi   = mult * (last_xpos - xpos);
    camera->pitch = CLAMP(camera->pitch + delta_theta, -89.99, 89.99);
    camera->yaw += delta_phi;
    last_xpos = xpos; last_ypos = ypos;

    float phi = (2 * M_PI) * (camera->yaw / 360);
    float theta = (2 * M_PI) * ((90 + camera->pitch) / 360);

    vec3 p = camera->position;
    float r = length(p);
    r -= (r / 2) * scroll;
    camera->position = {
        r * sinf(theta) * sinf(phi),
        r * cosf(theta),
        r * sinf(theta) * cosf(phi),
    };

    camera->view = view_matrix(camera->position, (camera->yaw * M_PI / 180.0f), (camera->pitch * M_PI / 180.0f));
}

void update_camera(Camera* camera, GLFWwindow* window, float dt) {
    if(glfwGetKey(window, GLFW_KEY_LEFT))   camera->yaw += dt * camera->rot_vel;
    if(glfwGetKey(window, GLFW_KEY_RIGHT))  camera->yaw -= dt * camera->rot_vel;
    if(glfwGetKey(window, GLFW_KEY_UP))   camera->pitch += dt * camera->rot_vel;
    if(glfwGetKey(window, GLFW_KEY_DOWN)) camera->pitch -= dt * camera->rot_vel;
    camera->pitch = CLAMP(camera->pitch, -89.99, 89.99);

    vec3 front = {
        -cosf(to_radians(camera->pitch)) * sinf(to_radians(camera->yaw)),
        sinf(to_radians(camera->pitch)),
        -cosf(to_radians(camera->pitch)) * cosf(to_radians(camera->yaw))
    };
    vec3 left = {
        -cosf(to_radians(camera->pitch)) * sinf(to_radians(camera->yaw + 90)),
        0,
        -cosf(to_radians(camera->pitch)) * cosf(to_radians(camera->yaw + 90))
    };
    vec3 dir = {0};
    if(glfwGetKey(window, GLFW_KEY_W)) dir += front;
    if(glfwGetKey(window, GLFW_KEY_S)) dir -= front;
    if(glfwGetKey(window, GLFW_KEY_A)) dir += left;
    if(glfwGetKey(window, GLFW_KEY_D)) dir -= left;
    
    if(camera->locked) { dir[1] = 0; }
    camera->position += dt * camera->vel * (length(dir) != 0 ? normalize(dir) : dir);
    if(glfwGetKey(window, GLFW_KEY_SPACE))        camera->position[1] += dt * camera->vel;
    if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL)) camera->position[1] -= dt * camera->vel;
    camera->view = view_matrix(camera->position, (camera->yaw * M_PI / 180.0f), (camera->pitch * M_PI / 180.0f));
}
