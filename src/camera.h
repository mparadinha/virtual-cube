#pragma once

#include "gl_math.h"
#include "shaders.h"

#include <string>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

struct Camera {
    float yaw, pitch;

    mat4 projection, view;
    vec3 position;

    bool locked = false;

    float vel = 6;
    float rot_vel = 45;
};

void set_camera_uniforms(Camera camera, Shader shader, const char* p_str = "p", const char* v_str = "v");
void update_camera_centered(Camera* camera, GLFWwindow* window, float scroll, float dt);
void update_camera(Camera* camera, GLFWwindow* window, float dt);
