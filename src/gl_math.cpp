#include "gl_math.h"

#include <stdio.h>

float to_radians(float degrees) { return degrees * M_PI / 180.0f; }

vec3 cross_product(vec3 a, vec3 b) {
    return {
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]
    };
}

mat4 operator-(mat4 m) {
    mat4 res = {0};
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            res[i][j] = -m[i][j];
        }
    }
    return res;
}

mat4 operator-(mat4 a, mat4 b) {
    mat4 res = {0};
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            res[i][j] = a[i][j] - b[i][j];
        }
    }
    return res;
}

mat4 operator+(mat4 a, mat4 b) {
    mat4 res = {0};
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            res[i][j] = a[i][j] + b[i][j];
        }
    }
    return res;
}

void print(mat4 m) {
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            printf("%s%4.4f ", m[j][i] < 0 ? "":" ", m[j][i]);
        }
        printf("\n");
    }
}

mat4 identity_matrix() {
    mat4 identity = {0};
    for(auto i = 0; i < 4; i++) identity[i][i] = 1;
    return identity;
}

mat4 transpose(mat4 m) {
    mat4 t = {0};
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            t[i][j] = m[j][i];
        }
    }
    return t;
}

mat4 inverse(mat4 _m) {
    // the code I found here https://stackoverflow.com/a/1148405
    // assumed that the matrices were just an array of 16 float
    // so I used some pointer casting fuckery to avoid writting this shit again (sorry)
    // we can do this because we KNOW that our mat4 is just a struct that has 16 floats
    // in it so in memory its the same as float[16]

    float _i[16];
    float* m = (float*) &_m;
    _i[0] = m[5]  * m[10] * m[15] - m[5]  * m[11] * m[14] - m[9]  * m[6] * m[15] + m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7]  * m[10];
    _i[4] = -m[4]  * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] - m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
    _i[8] = m[4]  * m[9] * m[15] -  m[4]  * m[11] * m[13] -  m[8]  * m[5] * m[15] +  m[8]  * m[7] * m[13] +  m[12] * m[5] * m[11] - m[12] * m[7] * m[9];
    _i[12] = -m[4]  * m[9] * m[14] +  m[4]  * m[10] * m[13] + m[8]  * m[5] * m[14] -  m[8]  * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];
    _i[1] = -m[1]  * m[10] * m[15] + m[1]  * m[11] * m[14] + m[9]  * m[2] * m[15] - m[9]  * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
    _i[5] = m[0]  * m[10] * m[15] - m[0]  * m[11] * m[14] - m[8]  * m[2] * m[15] + m[8]  * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];
    _i[9] = -m[0]  * m[9] * m[15] + m[0]  * m[11] * m[13] + m[8]  * m[1] * m[15] - m[8]  * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];
    _i[13] = m[0]  * m[9] * m[14] -m[0]  * m[10] * m[13] - m[8]  * m[1] * m[14] + m[8]  * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];
    _i[2] = m[1]  * m[6] * m[15] - m[1]  * m[7] * m[14] - m[5]  * m[2] * m[15] + m[5]  * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];
    _i[6] = -m[0]  * m[6] * m[15] + m[0]  * m[7] * m[14] + m[4]  * m[2] * m[15] - m[4]  * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];
    _i[10] = m[0]  * m[5] * m[15] - m[0]  * m[7] * m[13] - m[4]  * m[1] * m[15] + m[4]  * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];
    _i[14] = -m[0]  * m[5] * m[14] + m[0]  * m[6] * m[13] + m[4]  * m[1] * m[14] - m[4]  * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];
    _i[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] - m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];
    _i[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] + m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];
    _i[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] - m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];
    _i[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] + m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

    float det = m[0] * _i[0] + m[1] * _i[4] + m[2] * _i[8] + m[3] * _i[12];
    mat4 inv = *((mat4*) &_i);
    inv /= det;

    // perhaps most suprising of all, this shit seems to work!
    return inv;
}

mat4 operator*(mat4 a, mat4 b) {
    mat4 res = {0};
    for(auto i = 0; i < 4; i++) {
        for(auto j = 0; j < 4; j++) {
            vec4 line = {0};
            for(auto k = 0; k < 4; k++) line[k] = a[k][j];
            res[i][j] = dot_product(b.columns[i], line);
        }
    }

    return res;
}

vec4 operator*(mat4 m, vec4 v) {
    vec4 res = {0};
    mat4 trans = transpose(m);
    for(auto i = 0; i < 4; i++) res[i] = dot_product(v, trans.columns[i]);
    return res;
}

mat4 projection_matrix(float fov, float ratio, float near, float far) {
    mat4 proj = {0};
 
    // NOTE: see https://www.songho.ca/opengl/gl_projectionmatrix.html
    // for the math and reasons behind these numbers
    float tan_fov_over2 = tan(fov * M_PI / 360.0f);

    proj[0][0] = 1.0f / (ratio * tan_fov_over2);
    proj[1][1] = 1.0f / tan_fov_over2;
    proj[2][2] = (near + far) / (near - far);
    proj[2][3] = -1.0f;
    proj[3][2] = (2.0f * near * far) / (near - far);

    return proj;
}

mat4 ortho_projection_matrix(float left, float right, float bottom, float top, float near, float far) {
    // see same link as projection_matrix for the maths on this
    mat4 ortho = {0};
    ortho[0][0] = 2 / (right - left);
    ortho[1][1] = 2 / (top - bottom);
    ortho[2][2] = 2 / (near - far);
    ortho[3][3] = 1;
    ortho[3][0] = - (right + left) / (right - left);
    ortho[3][1] = - (top + bottom) / (top - bottom);
    ortho[3][2] = - (far + near) / (far - near);

    return ortho;
}

// note: yaw and pitch are in radians
// yaw starts at -x and rotates according to +y
// pitch is the angle with the xz plane
mat4 view_matrix(vec3 pos, float yaw, float pitch) {
    mat4 view = {0};

    vec3 x = { cosf(yaw), 0, -sinf(yaw) };
    vec3 y = { sinf(yaw) * sinf(pitch), cosf(pitch), cosf(yaw) * sinf(pitch) };
    vec3 z = { sinf(yaw) * cosf(pitch), -sinf(pitch), cosf(pitch) * cosf(yaw) };

    view.columns[0] = { x[0], y[0], z[0], 0 };
    view.columns[1] = { x[1], y[1], z[1], 0 };
    view.columns[2] = { x[2], y[2], z[2], 0 };
    view.columns[3] = { -dot_product(x, pos), -dot_product(y, pos), -dot_product(z, pos), 1 };

    return view;
}

mat4 look_at_matrix(vec3 eye, vec3 center, vec3 up) {
    // stolen from https://github.com/HandmadeMath/Handmade-Math/blob/a9b08b9147a7f3a80f23b4ed6c1bc81735930f17/HandmadeMath.h#L3009
    mat4 look = {0};

    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross_product(f, up));
    vec3 u = cross_product(s, f);
    look[3] = {-dot_product(s, eye), -dot_product(u, eye), dot_product(f, eye), 1};
    look[2] = {s[2], u[2], -f[2], 0};
    look[1] = {s[1], u[1], -f[1], 0};
    look[0] = {s[0], u[0], -f[0], 0};

    return look;
}

mat4 translation_matrix(vec3 delta) {
    mat4 res = identity_matrix();
    res.columns[3] = { delta[0], delta[1], delta[2], 1 };
    return res;
}

mat4 scaling_matrix(vec3 scale) {
    mat4 res = {0};
    for(auto i = 0; i < 3; i++) res[i][i] = scale[i];
    res[3][3] = 1;
    return res;
}

// note: axis must be normalized
// note: angle is in radians
mat4 rotation_matrix(vec3 axis, float angle) {
    mat4 rot = {0};

    // got this stuff from this wikipedia page
    // https://www.wikiwand.com/en/Rotation_matrix#/Rotation_matrix_from_axis_and_angle
    float c = 1 - cosf(angle);
    rot.columns[0] = {
        cosf(angle) + axis[0] * axis[0] * c,
        axis[0] * axis[1] * c + axis[2] * sinf(angle),
        axis[0] * axis[1] * c - axis[1] * sinf(angle) };
    rot.columns[1] = {
        axis[0] * axis[1] * c - axis[2] * sinf(angle),
        cosf(angle) + axis[1] * axis[1] * c,
        axis[2] * axis[1] * c + axis[0] * sinf(angle) };
    rot.columns[2] = {
        axis[0] * axis[2] * c + axis[1] * sinf(angle),
        axis[1] * axis[2] * c - axis[0] * sinf(angle),
        cosf(angle) + axis[2] * axis[2] * c };
    rot[3][3] = 1;
    return rot;
}

quat operator*(float s, quat q) { return {s * q[0], s * q[1], s * q[2], s * q[3]}; }
quat operator/(quat q, float s) { return (1 / s) * q; }

float length(quat q) { return sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]); }
quat normalize(quat q) { return q / length(q); }

mat4 to_matrix(quat q) {
    mat4 res = {0};
    // got this from https://github.com/HandmadeMath/Handmade-Math/blob/45c91702a910f7b8df0ac90f84667f6d94ffb9d3/HandmadeMath.h#L2442
    res[0][0] = 1 - 2 * (q[1] * q[1] + q[2] * q[2]);
    res[0][1] = 2 * (q[0] * q[1] + q[3] * q[2]);
    res[0][2] = 2 * (q[0] * q[2] - q[3] * q[1]);
    res[1][0] = 2.0f * (q[0] * q[1] - q[3] * q[2]);
    res[1][1] = 1.0f - 2.0f * (q[0] * q[0] + q[2] * q[2]);
    res[1][2] = 2.0f * (q[1] * q[2] + q[3]* q[0]);
    res[2][0] = 2.0f * (q[0] * q[2] + q[3]* q[1]);
    res[2][1] = 2.0f * (q[1] * q[2] - q[3]* q[0]);
    res[2][2] = 1.0f - 2.0f * (q[0]* q[0] + q[1]* q[1]);
    res[3][3] = 1;

    return res;
}
