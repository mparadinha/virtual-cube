#pragma once

#include <vector>
#include <string>

struct Buffer {
    uint8_t* data;
    size_t size;
    size_t pos;
};

void print_debug(const char* fmt, ...);
Buffer read_whole_file(const char* filepath);
